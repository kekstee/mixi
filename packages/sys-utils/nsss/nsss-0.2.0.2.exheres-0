# Copyright 2018 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="An implementation of a subset of the pwd.h, group.h and shadow.h family of functions, performing user database access on Unix systems."
HOMEPAGE="http://skarnet.org/software/${PN}/"
DOWNLOADS="http://skarnet.org/software/${PN}/${PNV}.tar.gz"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    static
"

DEPENDENCIES="
    build+run:
        dev-libs/skalibs[>=2.13.0.0]
    test:
        sys-apps/s6[>=2.11.2.0]
    suggestion:
        sys-apps/s6[>=2.11.2.0] [[ description = [ Using s6-ipcserver to launch nsss is recommended by upstream. ] ]]
"

# CROSS_COMPILE: because configure only expects executables to be prefixed if build != host
# REALCC: because the makefile prefixes configure's CC with $(CROSS_COMPILE)
DEFAULT_SRC_COMPILE_PARAMS=(
    REALCC=${CC}
    CROSS_COMPILE=$(exhost --target)-
)

src_configure()
{
    local args=(
        --enable-shared
        --with-lib=/usr/$(exhost --target)/lib
        --with-dynlib=/usr/$(exhost --target)/lib
        $(option_enable static allstatic)
    )

    [[ $(exhost --target) == *-gnu* ]] || \
        args+=( $(option_enable static static-libc) )

    econf "${args[@]}"
}

src_test()
{
    edo mkdir test-lib
    for lib in nsss nsssd; do
        edo ln -s ../lib${lib}.so.xyzzy test-lib/lib${lib}.so.$(ever range 1-2 ${PV})
    done

    esandbox allow_net "unix:${WORK}/.test-switch-socket"
    esandbox allow_net "unix:${WORK}/.test-nsssd-switch-socket"
    LD_LIBRARY_PATH=${WORK}/test-lib \
        default
    esandbox disallow_net "unix:${WORK}/.test-nsssd-switch-socket"
    esandbox disallow_net "unix:${WORK}/.test-switch-socket"
}

src_install()
{
    default

    dodoc -r examples

    insinto /usr/share/doc/${PNVR}/html
    doins doc/*
}

